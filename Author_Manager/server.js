const { response, request, json } = require('express');
const express = require('express');
const Joi = require('joi');


const schema = Joi.object({
    name: Joi.string().pattern(new RegExp('^[A-Za-z ]{5,30}$')),
    bookname: Joi.string().pattern(new RegExp('^[A-Za-z0-9 ]{5,50}$')),
    dob: Joi.string().regex(/^([0-9]{2})\-([0-9]{2})\-([0-9]{4})$/),
    description: Joi.string().min(30).max(150)

});
const app = express();
const PORT = 3000;

app.use(express.json());

app.listen(PORT, () => console.log(`Express server currently running on port ${PORT}`));
app.get('/', (request, response) => {
    response.send('Welcome to the Author Manager App');
});


let authorDetails = [{
        "name": "Mark",
        "dob": "23-02-1990",
        "id": Math.random().toString(36).substr(2, 9),
        "books": []

    }, {
        "name": "Charles",
        "dob": "07-10-1812",
        "id": Math.random().toString(36).substr(2, 9),
        "books": []

    },
    {
        "name": "Chetan",
        "dob": "22-04-1974",
        "id": Math.random().toString(36).substr(2, 9),
        "books": []


    }
]

//To display all author details
app.get('/authorlist', (request, response) => {
    response.json(authorDetails);
});
//To display a specific author's details
app.get('/authorlist/:id', (request, response) => {
    authorId = request.params.id;
    let resp = authorDetails.find(a => a.id == authorId)
    if (resp != null)
        response.json(resp);
    else
        response.status(404).send({ "Response Code": 320, "Error Message": "An author with the author ID does not exist." })

});

app.get('/allbooks', (request, response) => {
    response.json(allbooknames);
});

//To add an author
app.post('/newauthor', (request, response) => {

    const {
        error,
        value
    } = schema.validate({
        name: request.body.name,
        dob: request.body.dob
    });

    console.log(error)
    if (error == null) {


        authorDetails.push({
            "name": request.body.name,
            "dob": request.body.dob,
            "id": Math.random().toString(36).substr(2, 9),
            "books": []
        })

        response.json(authorDetails.slice(-1))
    } else {
        response.status(406).send({ "Response Code": 319, "Error Message": "Input details do not match the required format." });
    }

});

//To modify author
app.put('/changeauthordetails/:id', (request, response) => {
    const authorid = request.params.id;
    const validid = authorDetails.some(x => x.id == authorid)

    const {
        error,
        value
    } = schema.validate({
        name: request.body.newname,
        dob: request.body.newdob
    });

    if (validid && error == undefined) {
        var index = -1;
        var filteredRes = authorDetails.find(function(item, i) {
            if (item.id === authorid) {
                index = i;
                return i;
            }
        });

        const newname = request.body.newname;
        const newdob = request.body.newdob;
        authorDetails[index].name = newname;
        authorDetails[index].dob = newdob;

        response.json(authorDetails[index])

    } else {

        if (validid && error != undefined) {
            response.status(406).send({ "Response Code": 319, "Error Message": "Input details do not match the required format." });
        } else {
            response.status(404).send({ "Response Code": 320, "Error Message": "An author with the author ID does not exist." })

        }

    }
});

//Delete an author
app.delete('/deleteauthor/:id', (request, response) => {

    const authorid = request.params.id;
    const validaid = authorDetails.some(a => a.id == authorid)
    if (validaid) {

        authorDetails = authorDetails.filter(a => a.id != authorid)
        response.status(202).json({ "Response Code": 318, "Message": "The author profile has been deleted successfully." })
    } else {
        response.status(404).json({ "Response Code": 320, "Error Message": "An author with the author ID does not exist." })
    }

});

//Add Book

app.post("/addbook/:id", (request, response) => {

    const authorid = request.params.id;
    const validaid = authorDetails.some(x => x.id == authorid)
    const bookname = String(request.body.bookname);
    const pdate = String(request.body.bookpublishdate);
    const description = String(request.body.description);
    let newbookdetails = {}
    const {
        error,
        value
    } = schema.validate({
        bookname: bookname,
        dob: pdate,
        description: description
    });


    if (validaid && error == undefined) {
        var index = -1;
        var filteredRes = authorDetails.find(function(item, i) {
            if (item.id == authorid) {
                index = i;
                return i;
            }
        });


        newbookdetails["bookname"] = bookname;
        newbookdetails["bookpublishdate"] = pdate;
        newbookdetails["description"] = description;
        newbookdetails["bookid"] = Math.random().toString(36).substr(2, 9);

        authorDetails[index].books.push(newbookdetails);

        response.json(authorDetails[index].books.slice(-1))
    } else {
        if (validaid && error != undefined) {
            response.status(406).json({ "Response Code": 319, "Error Message": "Input details do not match the required format." });
        } else {
            response.status(404).json({ "Response Code": 320, "Error Message": "An author with the author ID does not exist." })

        }




    }
});


//Modify Book
app.put('/changebookdetails/:id', (request, response) => {
    const authorid = request.params.id;
    const bookidtoedit = request.body.bookid;
    const {
        error,
        value
    } = schema.validate({
        bookname: request.body.newname,
        dob: request.body.newdate,
        description: request.body.newdescription
    });

    var index = -1;
    var filteredRes = authorDetails.find(function(item, i) {
        if (item.id == authorid && item.books.some(a => a.bookid == bookidtoedit)) {
            index = i;
            return i;
        }
    });
    var bookindex = -1;
    var filteredRes = authorDetails[index].books.find(function(item, i) {
        if (item.bookid == bookidtoedit) {
            bookindex = i;
            return i;
        }
    });

    console.log(error)
    if (index != -1 && error == undefined) {

        authorDetails[index].books[bookindex].bookname = request.body.newname
        authorDetails[index].books[bookindex].bookpublishdate = request.body.newdate
        authorDetails[index].books[bookindex].description = request.body.newdescription
        response.json(authorDetails[index].books[bookindex])

    } else {
        response.status(404).json({ "Response Code": 321, "Error Message": "A book with the bookID for this author does not exist." })
    }

});

//delete book
app.delete('/deletebook/:id', (request, response) => {

    const authorid = request.params.id;
    const bookidtodelete = request.body.bookid;
    var index = -1;
    var filteredRes = authorDetails.find(function(item, i) {
        if (item.id == authorid && item.books.some(a => a.bookid == bookidtodelete)) {
            index = i;
            return i;
        }
    });
    let bookExists = 0
    let validid = authorDetails.some((a) => a.id == authorid)
    try {
        bookExists = authorDetails[index].books.some((a) => a.bookid == bookidtodelete)
    } catch (err) {


    }

    if (validid && bookExists) {
        var bookindex = -1;
        var filteredRes = authorDetails[index].books.find(function(item, i) {
            if (item.bookid == bookidtodelete) {
                bookindex = i;
                return i;
            }
        });

        authorDetails[index].books = authorDetails[index].books.filter((a) => a.bookid != bookidtodelete)
        response.json(authorDetails[index]);
    } else {
        if (validid)
            response.status(404).json({ "Response Code": 321, "Error Message": "A book with the bookID for this author does not exist." })

        else
            response.status(404).json({ "Response Code": 320, "Error Message": "An author with the following ID does not exist." })
    }
});

//Delete all Books
app.delete("/deleteallbooks/:id", (request, response) => {

    const authorid = request.params.id;
    let index = -1;
    var filteredRes = authorDetails.find(function(item, i) {
        if (item.id == authorid) {
            index = i;
            return i;
        }
    })
    let booksExist = -1
    try { booksExist = authorDetails[index].books.some(a => a) } catch {

    }
    if (booksExist == true) {

        authorDetails[index].books = []

        response.status(202).json({ "Response Code": 317, "Message": "The author's books have been deleted successfully." })

    } else {
        if (index != -1)
            response.status(404).json({ "Response Code": 323, "Error Message": "This author has no books written to his name." })

        else
            response.status(404).json({ "Response Code": 320, "Error Message": "An author with the following ID does not exist." })
    }

});
